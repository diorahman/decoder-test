#include "decodingthread.h"

DecodingThread::DecodingThread(QObject *parent, const QString & filename, const int & frameRate) :
    QObject(parent)
{
    m_decoder = new Decoder(this);
    m_timer = new QTimer(this);
    connect(m_timer, &QTimer::timeout, this, &DecodingThread::nextFrame);

    m_filename = filename;
    m_frameRate = frameRate;
}

void DecodingThread::play()
{
    m_timer->stop();
    m_timer->setInterval(1000/m_frameRate);
    m_decoder->close();
    m_decoder->openFile(m_filename);
    m_timer->start();
}

void DecodingThread::stop()
{
    m_timer->stop();
    m_decoder->close();
}

void DecodingThread::nextFrame()
{
    if(!m_decoder->seekNextFrame()){
        m_timer->stop();
        m_decoder->seekFrame(0);
        m_timer->start();
    }

    int i,j;
    m_decoder->getFrame(m_currentFrame, &i, &j);
    emit currentFrame(m_currentFrame);
}


QString DecodingThread::filename() const
{
    return m_filename;
}

void DecodingThread::setFilename(const QString & value)
{
    m_filename = value;
}
