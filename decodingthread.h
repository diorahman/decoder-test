#ifndef DECODINGTHREAD_H
#define DECODINGTHREAD_H

#include <QObject>
#include <QTimer>
#include <QImage>
#include "decoder.h"

class DecodingThread : public QObject
{
    Q_OBJECT
public:
    explicit DecodingThread(QObject *parent = 0, const QString& filename = "", const int & frameRate = 25 );

    QString filename() const;
    void setFilename(const QString & value);

signals:
    void currentFrame(QImage image);
    
public slots:
    void play();
    void stop();

private slots:
    void nextFrame();

private:
    QTimer * m_timer;
    Decoder * m_decoder;
    QImage m_currentFrame;

    QString m_filename;
    int m_frameRate;
    
};

#endif // DECODINGTHREAD_H
