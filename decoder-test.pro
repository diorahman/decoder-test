# Add more folders to ship with the application, here
folder_01.source = qml/decoder-test
folder_01.target = qml
DEPLOYMENTFOLDERS = folder_01

# Additional import path used to resolve QML modules in Creator's code model
QML_IMPORT_PATH =

# If your application uses the Qt Mobility libraries, uncomment the following
# lines and add the respective components to the MOBILITY variable.
# CONFIG += mobility
# MOBILITY +=

# The .cpp file which was generated for your project. Feel free to hack it.
SOURCES += main.cpp

# Please do not modify the following two lines. Required for deployment.
include(qtquick2applicationviewer/qtquick2applicationviewer.pri)
qtcAddDeployment()

HEADERS += \
    decoder.h

SOURCES += \
    decoder.cpp

HEADERS += \
    ffmpeg.h

LIBS += -L/usr/local/Cellar/schroedinger/1.0.11/lib
LIBS += -L/usr/local/Cellar/opus/1.0.2/lib
LIBS += -L/usr/local/Cellar/freetype/2.4.11/lib
LIBS += -L/usr/local/Cellar/libass/0.10.1/lib
LIBS += -lavcodec -lavformat -lavutil -lswscale -lswresample
LIBS += -lxvidcore -lx264 -lvpx -lvorbisenc -lvorbis -logg -lvo-aacenc -ltheoraenc -ltheoradec -logg -lspeex -lschroedinger-1.0 -lopus -lopenjpeg -lopencore-amrwb -lopencore-amrnb -lmp3lame -lfdk-aac -lfaac -lcelt0 -lass -laacplus
LIBS += -liconv -lfreetype
LIBS += -lm -lbz2 -lz
LIBS += -lssl -lcrypto

HEADERS += \
    test.h

SOURCES += \
    test.cpp

HEADERS += \
    video.h

SOURCES += \
    video.cpp

HEADERS += \
    decodingthread.h

SOURCES += \
    decodingthread.cpp
