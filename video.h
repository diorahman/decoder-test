#ifndef VIDEO_H
#define VIDEO_H

#include <QtQuick/QQuickItem>
#include <QtQuick/QSGGeometry>
#include <QtQuick/QSGTextureMaterial>
#include <QtQuick/QSGSimpleTextureNode>
#include <QImage>
#include <QTimer>
#include <QThread>

#include "decodingthread.h"

class Video : public QQuickItem
{
    Q_OBJECT

    Q_PROPERTY(int period WRITE setPeriod READ period NOTIFY periodChanged)
    Q_PROPERTY(QString filename WRITE setFilename READ filename NOTIFY filenameChanged)

public:
    explicit Video(QQuickItem *parent = 0);
    ~Video();

    int period() const;
    void setPeriod(int value);

    QString filename() const;
    void setFilename(const QString & value);

protected:
    virtual QSGNode *updatePaintNode(QSGNode *, UpdatePaintNodeData *);
    
signals:
    void periodChanged();
    void filenameChanged();
    
private:
    QSGGeometry m_geometry;
    QSGTexture * m_texture;
    QSGSimpleTextureNode * m_node;

    QImage m_image;
    int m_period;
    QString m_filename;

    DecodingThread * m_decoder;
    QThread * m_thread;

private slots:
    void updateFrame(QImage image);
};

#endif // VIDEO_H
