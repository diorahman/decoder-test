#include <QtGui/QGuiApplication>
#include "qtquick2applicationviewer.h"
#include "video.h"

int main(int argc, char *argv[])
{
    QGuiApplication app(argc, argv);

    qmlRegisterType<Video>("com.rockybars.components", 1, 0, "Video");

    QtQuick2ApplicationViewer viewer;
    viewer.setMainQmlFile(QStringLiteral("qml/decoder-test/main.qml"));
    viewer.showExpanded();

    return app.exec();
}
