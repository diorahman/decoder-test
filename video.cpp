#include "video.h"
#include <QtQuick/QQuickWindow>

#include <QDebug>

Video::Video(QQuickItem *parent) :
    QQuickItem(parent),
    m_geometry(QSGGeometry::defaultAttributes_TexturedPoint2D(), 4),
    m_texture(0),
    m_node(0),
    m_period(25)
{
    setFlag(ItemHasContents);
    m_decoder = new DecodingThread();
    m_thread = new QThread(this);

    m_decoder->moveToThread(m_thread);

    connect(m_thread, &QThread::started, m_decoder, &DecodingThread::play);
    connect(m_thread, &QThread::finished, m_decoder, &DecodingThread::deleteLater);
    connect(m_decoder, &DecodingThread::currentFrame, this, &Video::updateFrame);
}

Video::~Video()
{
    m_thread->exit();
    m_thread->wait();
}

QSGNode *Video::updatePaintNode(QSGNode * oldNode, QQuickItem::UpdatePaintNodeData *)
{
    if (width() <= 0 || height() <= 0) {
            delete oldNode;
            return 0;
    }

    if(m_texture) delete m_texture;
    m_texture = window()->createTextureFromImage(m_image);
    m_texture->setFiltering(QSGTexture::Nearest);
    m_texture->setHorizontalWrapMode(QSGTexture::ClampToEdge);
    m_texture->setVerticalWrapMode(QSGTexture::ClampToEdge);

    m_node = static_cast<QSGSimpleTextureNode *>(oldNode);

    if(!m_node){
        m_node = new QSGSimpleTextureNode();
        m_node->setFiltering(QSGTexture::None);
        m_node->setFlag(QSGNode::UsePreprocess);
    }

    QSGGeometry::updateTexturedRectGeometry(&m_geometry, QRectF(0, 0, width(), height()), QRectF(0, 0, 1, 1));
    m_node->setGeometry(&m_geometry);
    m_node->setTexture(m_texture);
    return m_node;
}

int Video::period() const
{
    return m_period;
}

void Video::setPeriod(int value)
{
    if(m_period != value){
        m_period = value;
        emit periodChanged();
    }
}

QString Video::filename() const
{
    return m_filename;
}

void Video::setFilename(const QString & value)
{
    if(m_filename != value){
        m_filename = value;
        m_decoder->setFilename(m_filename);
        m_thread->start();

        emit filenameChanged();
    }
}


void Video::updateFrame(QImage image)
{
    m_image = image;
    this->update();
}
