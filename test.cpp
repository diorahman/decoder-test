#include "test.h"
#include "decoder.h"

#include <QImage>
#include <QDebug>

Test::Test(QObject *parent) :
    QObject(parent)
{
    decoder = new Decoder(this);
    decoder->openFile("/Volumes/BOOTCAMP/Data/background.avi");
    decoder->seekNextFrame();
    int i,j;
    QImage img;

    decoder->getFrame(img, &i, &j);

    qDebug() << img.width() << " - " << img.height();

    img.save("test.png");

    qDebug() << "i: " << i << " - j: " << j;
}
