#include "decoder.h"
#include <QDebug>

Decoder::Decoder(QObject *parent) :
    QObject(parent)
{
    initVars();
    initCodec();
}

Decoder::~Decoder()
{
    close();
}

bool Decoder::initCodec()
{
    ffmpeg::av_register_all();
    return true;
}

void Decoder::initVars()
{
    ok=false;
    pFormatCtx=0;
    pCodecCtx=0;
    pCodec=0;
    pFrame=0;
    pFrameRGB=0;
    buffer=0;
    img_convert_ctx=0;
    optionsDict = 0;
}

bool Decoder::decodeSeekFrame(int after)
{
    if(!ok)
       return false;

    if( after!=-1 && ( LastFrameOk==true && after>=LastLastFrameNumber && after <= LastFrameNumber))
    {
       // This is the frame we want to return

       // Compute desired frame time
       ffmpeg::AVRational millisecondbase = {1, 1000};
       DesiredFrameTime = ffmpeg::av_rescale_q(after,pFormatCtx->streams[videoStream]->time_base,millisecondbase);

       //printf("Returning already available frame %d @ %d. DesiredFrameTime: %d\n",LastFrameNumber,LastFrameTime,DesiredFrameTime);

       return true;
    }

    bool done = false;
    while(!done)
    {
        if(ffmpeg::av_read_frame(pFormatCtx, &packet)<0) return false;

        if(packet.stream_index==videoStream)
        {
            int frameFinished;
            ffmpeg::avcodec_decode_video2(pCodecCtx,pFrame,&frameFinished,&packet);

            if(frameFinished)
            {

                ffmpeg::AVRational millisecondbase = {1, 1000};
                int f = packet.dts;
                int t = ffmpeg::av_rescale_q(packet.dts,pFormatCtx->streams[videoStream]->time_base,millisecondbase);
                if(LastFrameOk==false)
                {
                   LastFrameOk=true;
                   LastLastFrameTime=LastFrameTime=t;
                   LastLastFrameNumber=LastFrameNumber=f;
                }
                else
                {
                   // If we decoded 2 frames in a row, the last times are okay
                   LastLastFrameTime = LastFrameTime;
                   LastLastFrameNumber = LastFrameNumber;
                   LastFrameTime=t;
                   LastFrameNumber=f;
                }

                // Is this frame the desired frame?
                if(after==-1 || LastFrameNumber>=after)
                {
                   // It's the desired frame

                   // Convert the image format (init the context the first time)
                   int w = pCodecCtx->width;
                   int h = pCodecCtx->height;
                   //img_convert_ctx = ffmpeg::sws_getCachedContext(img_convert_ctx,w, h, pCodecCtx->pix_fmt, w, h, ffmpeg::PIX_FMT_RGB24, SWS_BICUBIC, NULL, NULL, NULL);
                   img_convert_ctx = ffmpeg::sws_getCachedContext(img_convert_ctx,w, h, pCodecCtx->pix_fmt, w, h, ffmpeg::PIX_FMT_BGRA, SWS_BICUBIC, NULL, NULL, NULL);

                   if(img_convert_ctx == NULL)
                   {
                      printf("Cannot initialize the conversion context!\n");
                      return false;
                   }
                   ffmpeg::sws_scale(img_convert_ctx, pFrame->data, pFrame->linesize, 0, pCodecCtx->height, pFrameRGB->data, pFrameRGB->linesize);

                   // Convert the frame to QImage
                   //LastFrame=QImage(w,h,QImage::Format_RGB888);
                   LastFrame=QImage(w,h,QImage::Format_ARGB32);

                   for(int y=0;y<h;y++)
                      //memcpy(LastFrame.scanLine(y),pFrameRGB->data[0]+y*pFrameRGB->linesize[0],w*3);
                       memcpy(LastFrame.scanLine(y),pFrameRGB->data[0]+y*pFrameRGB->linesize[0],w*4);

                   // Set the time
                   DesiredFrameTime = ffmpeg::av_rescale_q(after,pFormatCtx->streams[videoStream]->time_base,millisecondbase);
                   LastFrameOk=true;


                   done = true;

                } // frame of interest
            }
        }

        ffmpeg::av_free_packet(&packet);
    }

    return true;
}

bool Decoder::openFile(const QString &filename)
{
    LastLastFrameTime=INT_MIN;
    LastFrameTime=0;
    LastLastFrameNumber=INT_MIN;
    LastFrameNumber=0;
    DesiredFrameTime=DesiredFrameNumber=0;
    LastFrameOk=false;

    if(ffmpeg::avformat_open_input(&pFormatCtx, filename.toStdString().c_str(), NULL, NULL)!=0)
        return false; // Couldn't open file


    if(ffmpeg::avformat_find_stream_info(pFormatCtx, NULL)<0)
        return -1;

    ffmpeg::av_dump_format(pFormatCtx, 0, filename.toStdString().c_str(), 0);

    // Find the first video stream
    videoStream = -1;
    for(unsigned i = 0; i < pFormatCtx->nb_streams; i++)
        if(pFormatCtx->streams[i]->codec->codec_type == ffmpeg::AVMEDIA_TYPE_VIDEO) {
            videoStream = i;
            break;
    }

    if(videoStream==-1)
        return false; // Didn't find a video stream

    // Get a pointer to the codec context for the video stream
    pCodecCtx = pFormatCtx->streams[videoStream]->codec;

    // Find the decoder for the video stream
    pCodec = ffmpeg::avcodec_find_decoder(pCodecCtx->codec_id);
    if(pCodec == NULL) {
      fprintf(stderr, "Unsupported codec!\n");
      return -1; // Codec not found
    }

    // Open codec
    if(ffmpeg::avcodec_open2(pCodecCtx, pCodec, &optionsDict) < 0)
      return false; // Could not open codec

    // Allocate video frame
    pFrame = ffmpeg::avcodec_alloc_frame();

    // Allocate an AVFrame structure
    pFrameRGB = ffmpeg::avcodec_alloc_frame();

    if(pFrameRGB == NULL)
      return false;

    // Determine required buffer size and allocate buffer
    //numBytes = ffmpeg::avpicture_get_size( ffmpeg::PIX_FMT_RGB24, pCodecCtx->width, pCodecCtx->height);
    numBytes = ffmpeg::avpicture_get_size( ffmpeg::PIX_FMT_BGRA, pCodecCtx->width, pCodecCtx->height);
    buffer = (uint8_t *) ffmpeg::av_malloc(numBytes*sizeof(uint8_t));

    //avpicture_fill((ffmpeg::AVPicture *)pFrameRGB, buffer, ffmpeg::PIX_FMT_RGB24,
    //    pCodecCtx->width, pCodecCtx->height);

    avpicture_fill((ffmpeg::AVPicture *)pFrameRGB, buffer, ffmpeg::PIX_FMT_BGRA,
        pCodecCtx->width, pCodecCtx->height);


    ok=true;

    return true;
}

bool Decoder::seekNextFrame()
{
    bool ret = decodeSeekFrame(DesiredFrameNumber+1);

    if(ret)
       DesiredFrameNumber++;   // Only updates the DesiredFrameNumber if we were successful in getting that frame
    else
       LastFrameOk=false;      // We didn't find the next frame (e.g. seek out of range) - mark we don't know where we are.
    return ret;
}

bool Decoder::seekFrame(int64_t frame)
{

   if(!ok)
      return false;

   //printf("**** seekFrame to %d. LLT: %d. LT: %d. LLF: %d. LF: %d. LastFrameOk: %d\n",(int)frame,LastLastFrameTime,LastFrameTime,LastLastFrameNumber,LastFrameNumber,(int)LastFrameOk);

   // Seek if:
   // - we don't know where we are (Ok=false)
   // - we know where we are but:
   //    - the desired frame is after the last decoded frame (this could be optimized: if the distance is small, calling decodeSeekFrame may be faster than seeking from the last key frame)
   //    - the desired frame is smaller or equal than the previous to the last decoded frame. Equal because if frame==LastLastFrameNumber we don't want the LastFrame, but the one before->we need to seek there
   if( (LastFrameOk==false) || ((LastFrameOk==true) && (frame<=LastLastFrameNumber || frame>LastFrameNumber) ) )
   {
      //printf("\t avformat_seek_file\n");
      if(ffmpeg::avformat_seek_file(pFormatCtx,videoStream,0,frame,frame, AVSEEK_FLAG_FRAME)<0)
         return false;

      ffmpeg::avcodec_flush_buffers(pCodecCtx);

      DesiredFrameNumber = frame;
      LastFrameOk=false;
   }

   return decodeSeekFrame(frame);
}

bool Decoder::getFrame(QImage &img, int *effectiveframenumber, int *effectiveframetime, int *desiredframenumber, int *desiredframetime)
{
    img = LastFrame;

    if(effectiveframenumber)
       *effectiveframenumber = LastFrameNumber;
    if(effectiveframetime)
       *effectiveframetime = LastFrameTime;
    if(desiredframenumber)
       *desiredframenumber = DesiredFrameNumber;
    if(desiredframetime)
       *desiredframetime = DesiredFrameTime;

    // printf("getFrame. Returning valid? %s. Desired %d @ %d. Effective %d @ %d\n",LastFrameOk?"yes":"no",DesiredFrameNumber,DesiredFrameTime,LastFrameNumber,LastFrameTime);

    return LastFrameOk;
}

void Decoder::reset()
{
    seekFrame(0);
}

void Decoder::close()
{
   if(!ok)
      return;

   // Free the RGB image
   if(buffer)
      delete [] buffer;

   // Free the YUV frame
   if(pFrame)
      ffmpeg::av_free(pFrame);

   // Free the RGB frame
   if(pFrameRGB)
      ffmpeg::av_free(pFrameRGB);

   // Close the codec
   if(pCodecCtx)
      ffmpeg::avcodec_close(pCodecCtx);

   // Close the video file
   if(pFormatCtx)
       ffmpeg::avformat_close_input(&pFormatCtx);

   initVars();
}


