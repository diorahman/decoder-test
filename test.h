#ifndef TEST_H
#define TEST_H

#include <QObject>

class Decoder;

class Test : public QObject
{
    Q_OBJECT
public:
    explicit Test(QObject *parent = 0);
    
signals:
    
public slots:

private:
    Decoder * decoder;
    
};

#endif // TEST_H
