#ifndef DECODER_H
#define DECODER_H

#include <QObject>
#include <QImage>
#include "ffmpeg.h"


class Decoder : public QObject
{
    Q_OBJECT
public:
    explicit Decoder(QObject *parent = 0);
    ~Decoder();


protected:
    int videoStream;

    ffmpeg::AVFormatContext *pFormatCtx;
    ffmpeg::AVCodecContext  *pCodecCtx;
    ffmpeg::AVCodec         *pCodec;
    ffmpeg::AVFrame         *pFrame;
    ffmpeg::AVFrame         *pFrameRGB;
    ffmpeg::AVPacket        packet;
    ffmpeg::SwsContext      *img_convert_ctx;
    ffmpeg::AVDictionary    *optionsDict;
    uint8_t                 *buffer;
    int                     numBytes;

    // State infos for the wrapper
    bool ok;
    QImage LastFrame;
    int LastFrameTime,LastLastFrameTime,LastLastFrameNumber,LastFrameNumber;
    int DesiredFrameTime,DesiredFrameNumber;
    bool LastFrameOk;                // Set upon start or after a seek we don't have a frame yet

    // Initialization functions
    virtual bool initCodec();
    virtual void initVars();

    virtual bool decodeSeekFrame(int after);
    
signals:
    
public slots:

public:
    virtual bool openFile(const QString & filename);
    virtual bool seekNextFrame();
    virtual bool seekFrame(int64_t frame);
    virtual bool getFrame(QImage& img, int *effectiveframenumber=0, int *effectiveframetime=0, int *desiredframenumber=0, int *desiredframetime=0);
    virtual void reset();
    virtual void close();
    
};

#endif // DECODER_H
